<?php

$info = parse_url($url);
//$domain = $_SERVER['HTTP_HOST']; //$info['host'];
$domain = $info['host'];

$id="";
$wurcs="";

if(!empty($_REQUEST)){
  $id = urlencode($_REQUEST['id']);
  $wurcs = $_REQUEST['wurcs'];
}

// CAN
$dataseturl = $domain."/";
if (strpos($id, 'CAN:') !== false) {
  $id = str_replace("CAN:", "", $id);
  $dataseturl = "/CAN/entry.php?id=".$id;
}

// GlyTouCan
if (preg_match("/^G[0-9]{5}[a-zA-Z]{2}/", $id)) {
  $dataseturl = $domain . "/Glycans/pdb-gtc.php?id=" . $id;
}

// Protein
$reg = "/^[0-9][A-Za-z0-9]{3}$/";
if (preg_match($reg, $id)) {
  $dataseturl = $domain . "/Proteins/entry.php?id=" . $id;
}

// Protein
$reg = "/^TCP:[0-9][A-Za-z0-9]{3}$/";
if (preg_match($reg, $id)) {
  $id = str_replace("TCP:","",$id);
  $id = str_replace("TCarp:", "", $id);
  $dataseturl = $domain . "/Proteins/entry.php?id=" . $id;
}

// Uniprot
// https://glyconavi.org/Proteins/pro_table.php?id=P00718
$reg = '/^([A-N,R-Z][0-9]([A-Z][A-Z, 0-9][A-Z, 0-9][0-9]){1,2})|([O,P,Q][0-9][A-Z, 0-9][A-Z, 0-9][A-Z, 0-9][0-9])(\.\d+)?$/';
if (preg_match($reg, $id)) {
  $dataseturl = $domain . "/Proteins/pro_table.php?id=" . $id;
}




// http://localhost:8888/Glycans/pdb-wurcs.php?wurcs=WURCS=2.0/2,2,1/[a1122h-1a_1-5][a211h-1b_1-5]/1-2/a2-b1
// https://glyconavi.org/Glycans/pdb-wurcs.php?wurcs=WURCS=2.0/2,7,6/[a2122h-1a_1-5][a2122h-1b_1-5]/1-1-1-1-1-1-2/a4-b1_b4-c1_c4-d1_d4-e1_e4-f1_f4-g1

if (strpos($wurcs, 'WURCS=') !== false) {
  $dataseturl = $domain . "/Glycans/pdb-wurcs.php?wurcs=" . urlencode($wurcs);
}



$url = "https://".$_SERVER['HTTP_HOST'].$dataseturl;
header('Location: ' . $url, true, 301);

exit;

?>
