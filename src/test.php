<?php
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>GlycoNAVI Site</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="header">Header</div>
  <div class="main">
    <h1>GlycoNAVI</h1>
    <p>Contents</p>
    <img src="https://glyconavi.org/img/GlycoNAVI.png">
  </div>
  <div class="footer">
    <span>Footer</span>
    <a href="#">link</a>
  </div>
</body>
</html>
